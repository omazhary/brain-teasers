const quicksort = require('./quicksort');

const unsortedArray = [1, 4, 2, 4, 6, 3, 17, 25, 89, 0, 100, -16, -132];
const sortedArray = [-132, -16, 0, 1, 2, 3, 4, 4, 6, 17, 25, 89, 100];

describe('quicksort', () => {
    it('should sort an array', () => {
        const quicksortedArray = [...unsortedArray];
        quicksort(quicksortedArray, 0, unsortedArray.length - 1);
        expect(quicksortedArray).toEqual(sortedArray);
    });
});
