function swap(array, index1, index2) {
    const temp = array[index1];
    array[index1] = array[index2];
    array[index2] = temp;
}

function quicksort(inputArray, low, high) {
    if (low >= high) {
        return;
    }
    // assume pivot is always last element
    const pivot = inputArray[high];
    let i = low - 1;
    let j = low;
    while (j < high) {
        if (inputArray[j] <= pivot) {
            i++;
            swap(inputArray, i, j);
        }
        j++;
    }
    const newPivot = i + 1;
    swap(inputArray, newPivot, high);
    // recurse over partitions
    quicksort(inputArray, low, Math.max(0, newPivot - 1));
    quicksort(inputArray, newPivot + 1, high);
}

module.exports = quicksort;
