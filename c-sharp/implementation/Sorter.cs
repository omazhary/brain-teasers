﻿using System;

namespace Sorter
{
    /// <summary>
    /// An array sorting class.
    /// </summary>
    public class Sorter
    {
        private static void swap(int[] array, int index1, int index2) {
            (array[index2], array[index1]) = (array[index1], array[index2]);
        }

        public static void QuickSort(int[] array, int low, int high) {
            if (low >= high) {
                return;
            }
            // assume pivot is always last element
            int pivot = array[high];
            int i = low - 1;
            int j = low;
            while (j < high) {
                if (array[j] <= pivot) {
                    i++;
                    swap(array, i, j);
                }
                j++;
            }
            int newPivot = i + 1;
            swap(array, newPivot, high);
            // recurse over partitions
            QuickSort(array, low, Math.Max(0, newPivot - 1));
            QuickSort(array, newPivot + 1, high);
            return;
        }
    }
}