using Sorter;

namespace tests;

public class SorterTests
{
    readonly int[] unsortedArray = [1, 4, 2, 4, 6, 3, 17, 25, 89, 0, 100, -16, -132];
    readonly int[] sortedArray = [-132, -16, 0, 1, 2, 3, 4, 4, 6, 17, 25, 89, 100];

    [Fact]
    public void QuickSortTest()
    {
        int[] testArray = new int[unsortedArray.Length];
        unsortedArray.CopyTo(testArray, 0);
        Sorter.Sorter.QuickSort(testArray, 0, testArray.Length - 1);
        Assert.Equal(testArray, sortedArray);
    }

}